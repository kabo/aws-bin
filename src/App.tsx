import React, { lazy, Suspense } from 'react'
import { Container, Grid, Header } from 'semantic-ui-react'
import useHashParam from 'use-hash-param'
import Show from './Show'
import Upload from './Upload'

const
  { REACT_APP_SHOW_CREDIT } = process.env,
  Credit = lazy(() => (REACT_APP_SHOW_CREDIT !== 'nah') ? import('./Credit') : Promise.resolve({ default: () => (<div />) }))

const App = () => {
  const [ nonce ] = useHashParam('n', ''),
    [ enckey ] = useHashParam('k', ''),
    [ path ] = useHashParam('p', '')
  return (
    <div className="App">
      <Container>
        <header className="header">
          <Grid>
            <Grid.Column width={6}>
              <Header as="h1">Temporary share</Header>
            </Grid.Column>
            <Grid.Column width={10} textAlign="right">
              <Suspense fallback={<div />}>
                {<Credit />}
              </Suspense>
            </Grid.Column>
          </Grid>
        </header>
        {enckey
          ? <Show {...{ nonce, enckey, path }} />
          : <Upload />
        }
      </Container>
    </div>
  )
}

export default App
