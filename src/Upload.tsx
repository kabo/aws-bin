import Ralways from 'ramda/src/always'
import Rcond from 'ramda/src/cond'
import Requals from 'ramda/src/equals'
import RifElse from 'ramda/src/ifElse'
import Rmatch from 'ramda/src/match'
import Rpipe from 'ramda/src/pipe'
import Rprop from 'ramda/src/prop'
import RpropEq from 'ramda/src/propEq'
// import Rtap from 'ramda/src/tap'
import RT from 'ramda/src/T'
import React, { useEffect, useState } from 'react'
import Clipboard from 'react-clipboard.js'
import Resizer from 'react-image-file-resizer'
import { Button, Image, Input } from 'semantic-ui-react'
import { InputFile } from 'semantic-ui-react-input-file'
import nacl from 'tweetnacl'
import { v4 as uuidv4 } from 'uuid'

interface IKeyNonce {
  readonly key: string
  readonly nonce: string
  readonly path: string
}
interface IEncryptedContent extends IKeyNonce {
  readonly encrypted: Uint8Array
}
interface IRequest {
  readonly length: number
  readonly password: string
}

const
  lambdaUrl = process.env.REACT_APP_LAMBDA_URL,
  bucketPrefix = process.env.REACT_APP_BUCKET_PREFIX,
  getFormat = Rcond<string, string>([
    [ Requals('image/png'), Ralways('PNG') ],
    [ Requals('image/webp'), Ralways('WEBP') ],
    [ RT, Ralways('JPEG') ],
  ]),
  resize = (content?: File, output: string = 'base64') =>
    content ?
      new Promise((resolve, reject) => {
        try {
          Resizer.imageFileResizer(
            content,
            1600,
            1600,
            getFormat(content.type),
            80,
            0,
            resolve,
            output,
            100,
            100
          )
        } catch (e: any) {
          reject(e)
        }
      })
      : Promise.resolve(''),
  fromCharCode = (u8: number[]) => String.fromCharCode.apply(null, u8),
  base64encode = Rpipe<number[], string, string>(
    fromCharCode,
    btoa
  ),
  encrypt = (blob: Blob): Promise<IEncryptedContent> =>
    blob.arrayBuffer()
      .then((ab: ArrayBuffer) => {
        const unonce = nacl.randomBytes(nacl.secretbox.nonceLength),
          ukey = nacl.randomBytes(nacl.secretbox.keyLength),
          encrypted = nacl.secretbox(new Uint8Array(ab), unonce, ukey)
        return {
          key: base64encode(ukey as unknown as number[]),
          nonce: base64encode(unonce as unknown as number[]),
          path: `${bucketPrefix}${uuidv4()}`,
          encrypted,
        }
      }),
  extractKeyFromUrl = Rpipe<string, string[], string>(
    Rmatch(/^https:\/\/[^/]+\/([^?]+)/),
    Rprop<any, any>(1)
  ),
  getSignedUrl = ({ password, length }: IRequest): Promise<{ readonly url: string }> =>
    fetch(lambdaUrl!, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      credentials: 'omit',
      body: JSON.stringify({
        length,
        password,
      }),
    })
      .then(RifElse(
        RpropEq('ok', true),
        (r) => r.json(),
        (r) => r.json().then(Promise.reject.bind(Promise))
      )),
  upload = (body: Uint8Array) => ({ url }: { readonly url: string }) =>
    fetch(url, {
      method: 'PUT',
      body,
      credentials: 'omit',
      headers: {
        'Content-Type': 'application/octet-stream',
        'Cache-Control': 'no-cache',
      },
    })
      .then(() => extractKeyFromUrl(url)),
  generateLink = ({ path, key, nonce }: IKeyNonce) =>
    `${window.location.origin}/#?n=${encodeURIComponent(nonce)}&p=${path}&k=${encodeURIComponent(key)}`

const Upload = () => {
  const [ content, setContent ] = useState<File | undefined>(undefined)
  const [ newImage, setNewImage ] = useState<string>('')
  const [ link, setLink ] = useState<string>('')
  const [ password, setPassword ] = useState<string>('')
  const [ working, setWorking ] = useState(false)
  const handleContent = (event: React.ChangeEvent<HTMLInputElement>) =>
    setContent(event.target.files![ 0 ])
  const handleUpload = () => {
    setWorking(true);
    (resize(content, 'blob') as Promise<Blob>)
      .then(encrypt)
      .then(({ key, nonce, encrypted }: IEncryptedContent) =>
        getSignedUrl({ password, length: encrypted.length })
          .then(upload(encrypted))
          .then((path: string) => generateLink({ path, key, nonce })))
      .then(setLink)
      .catch((e: any) => {
        console.error(e)
      })
      .then(() => setWorking(false))
  }
  useEffect(() => {
    (resize(content, 'base64') as Promise<string>)
      .then(setNewImage)
  }, [ content ])
  return (
    <div className="Upload">
      <div className="form">
        <InputFile
          button={{}}
          input={{
            id: 'input-control-id',
            onChange: handleContent
          }}
        />
        <Input
          type="password"
          value={password}
          onChange={(_, { value }) => setPassword(value)}
          placeholder="Password"
        />
        <Button onClick={handleUpload} content="Upload" loading={working} />
      </div>
      <div className="link">
        {link && <Clipboard component={Button} data-clipboard-text={link}>Copy link</Clipboard>}
      </div>
      <div className="preview">
        {newImage && <Image src={newImage} centered />}
        {/*content && <p>{content.type}</p>*/}
      </div>
    </div>
  )
}

export default Upload
