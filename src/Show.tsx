import Rmap from 'ramda/src/map'
import Rpipe from 'ramda/src/pipe'
// import Rtap from 'ramda/src/tap'
import React, { useEffect, useState } from 'react'
import { Icon, Image } from 'semantic-ui-react'
import nacl from 'tweetnacl'

interface IKeyNonce {
  readonly enckey: string
  readonly nonce: string
  readonly path: string
}

const domainname = process.env.REACT_APP_DOMAINNAME,
  getImage = (path: string) =>
    fetch(`https://${domainname}/${path}`)
      .then((r) => r.ok ? r.arrayBuffer() : Promise.reject(r))
      .then((ab) => new Uint8Array(ab)),
  charCodeAt = (c: string) => c.charCodeAt(0),
  base64decode = Rpipe<string, string, number[], Uint8Array>(
    atob,
    Rmap(charCodeAt) as any,
    Uint8Array.from.bind(Uint8Array)
  ),
  decrypt = ({ enckey, nonce }: IKeyNonce) => (encrypted: Uint8Array) => {
    const unonce = base64decode(nonce),
      ukey = base64decode(enckey),
      decrypted = nacl.secretbox.open(encrypted, unonce, ukey)
    return new Blob([ decrypted! ])
  }

const Show = (params: IKeyNonce) => {
  const [ content, setContent ] = useState<string>('')
  const [ status, setStatus ] = useState<string>('')
  useEffect(() => {
    setStatus('loading')
    getImage(params.path)
      .then(decrypt(params))
      .then((b: Blob) => setContent(URL.createObjectURL(b)))
      .then(() => setStatus(''))
      .catch((err: any) => {
        console.error(err)
        setStatus(err.status === 404 ? '404 Image not found' : 'An error occured')
      })
  }, [ params ])
  return (
    <div className="Show">
      {status === 'loading'
        ? <Icon loading name="spinner" />
        : <div>{status}</div>}
      {content && <Image src={content} centered />}
    </div>
  )
}

export default Show
