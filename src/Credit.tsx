import React from 'react'

const Credit = () => (
  <span>
    By <a href="https://kabohub.co.nz/" target="_blank" rel="noopener noreferrer">KaboHub</a>
    &nbsp; |
    &nbsp;<a href="https://gitlab.com/kabo/aws-bin" target="_blank" rel="noopener noreferrer">Source</a>
  </span>
)

export default Credit
