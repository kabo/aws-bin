# aws-bin

## Prerequisites

- aws cli
- yarn
- make

## Quickstart

```sh
make install
echo 'STACK_NAME_EDGE_LAMBDA=aws-bin-edge-lambda' > .env
make deploy-cloudformation-edge-lambda
echo "LAMBDA_FUNCTION_ARN=$(make get-edge-lambda-arn)" >> .env
echo 'STACK_NAME_CLOUDFORMATION=aws-bin' >> .env
echo 'CONTENT_PREFIX=x/' >> .env
make deploy-cloudformation
echo "WEBSITE_BUCKET=$(make get-website-bucket)" >> .env
echo "REACT_APP_DOMAINNAME=$(make get-domainname)" >> .env
echo 'REACT_APP_BUCKET_PREFIX=x/' >> .env
echo 'REACT_APP_LAMBDA_URL=[the-url]' >> .env # see aws-bin-backend
make build
make sync
```
