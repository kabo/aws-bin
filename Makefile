JSBINS = ./node_modules/.bin

ifneq (,$(wildcard ./.env))
  include .env
  export
endif

all: build

guard-%:
	@if [ -z '${${*}}' ]; then echo 'ERROR: variable $* not set' && exit 1; fi

cmd-exists-%:
	@hash $(*) > /dev/null 2>&1 || \
		(echo "ERROR: '$(*)' must be installed and available on your PATH."; exit 1)

build: guard-REACT_APP_BUCKET_PREFIX guard-REACT_APP_LAMBDA_URL guard-REACT_APP_DOMAINNAME
	INLINE_RUNTIME_CHUNK=false GENERATE_SOURCEMAP=false $(JSBINS)/react-scripts build

sync: cmd-exists-aws guard-WEBSITE_BUCKET
	aws s3 sync \
		--cache-control 'max-age=60' \
		--delete \
		--exclude '*.map' \
		./build s3://${WEBSITE_BUCKET}

start: guard-REACT_APP_BUCKET_PREFIX guard-REACT_APP_LAMBDA_URL guard-REACT_APP_DOMAINNAME
	BROWSER=none $(JSBINS)/react-scripts start

test:
	$(JSBINS)/react-scripts test

deploy-cloudformation-edge-lambda: cmd-exists-aws guard-STACK_NAME_EDGE_LAMBDA
	aws cloudformation deploy \
		--region us-east-1 \
		--stack-name ${STACK_NAME_EDGE_LAMBDA} \
		--template-file cloudformation-edge-lambda.yml \
		--no-fail-on-empty-changeset \
		--capabilities CAPABILITY_IAM

get-edge-lambda-arn: cmd-exists-aws guard-STACK_NAME_EDGE_LAMBDA
	@aws cloudformation describe-stacks \
		--region us-east-1 \
		--stack-name ${STACK_NAME_EDGE_LAMBDA} \
		--output text \
		--query 'Stacks[0].Outputs[0].OutputValue'

deploy-cloudformation: cmd-exists-aws guard-CONTENT_PREFIX guard-LAMBDA_FUNCTION_ARN guard-STACK_NAME_CLOUDFORMATION
	aws cloudformation deploy \
		--region ap-southeast-2 \
		--stack-name ${STACK_NAME_CLOUDFORMATION} \
		--template-file cloudformation.yml \
		--no-fail-on-empty-changeset \
		--parameter-overrides ContentPrefix=${CONTENT_PREFIX} LambdaFunctionARN=${LAMBDA_FUNCTION_ARN}

get-website-bucket: cmd-exists-aws guard-STACK_NAME_CLOUDFORMATION
	@aws cloudformation describe-stacks \
		--region ap-southeast-2 \
		--stack-name ${STACK_NAME_CLOUDFORMATION} \
		--output text \
		--query 'Stacks[0].Outputs[0].OutputValue'

get-domainname: cmd-exists-aws guard-STACK_NAME_CLOUDFORMATION
	@aws cloudformation describe-stacks \
		--region ap-southeast-2 \
		--stack-name ${STACK_NAME_CLOUDFORMATION} \
		--output text \
		--query 'Stacks[0].Outputs[2].OutputValue' | \
		rev | cut -c2- | rev # get rid of . at the end

clean:
	rm -rf build/

install: cmd-exists-yarn
	yarn install

.PHONY: all build start sync test deploy-cloudformation-edge-lambda deploy-cloudformation clean install get-edge-lambda-arn get-website-bucket get-domainname
